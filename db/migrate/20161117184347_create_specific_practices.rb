class CreateSpecificPractices < ActiveRecord::Migration[5.0]
  def change
    create_table :specific_practices do |t|
      t.string :name
      t.string :abbrev
      t.text :description

      t.timestamps
    end
  end
end
