class CreateReferenceModels < ActiveRecord::Migration[5.0]
  def change
    create_table :reference_models do |t|
      t.string :abbrev
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
