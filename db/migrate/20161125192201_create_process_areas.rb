class CreateProcessAreas < ActiveRecord::Migration[5.0]
  def change
    create_table :process_areas do |t|
      t.string :abbrev
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
