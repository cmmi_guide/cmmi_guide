class CreateCapacityLevels < ActiveRecord::Migration[5.0]
  def change
    create_table :capacity_levels do |t|
      t.string :name
      t.string :abbrev
      t.text :description

      t.timestamps
    end
  end
end
