class CreateMaturityLevels < ActiveRecord::Migration[5.0]
  def change
    create_table :maturity_levels do |t|
      t.string :name
      t.string :abbrev
      t.text :description

      t.timestamps
    end
  end
end
