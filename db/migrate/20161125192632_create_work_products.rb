class CreateWorkProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :work_products do |t|
      t.string :name

      t.timestamps
    end
  end
end
