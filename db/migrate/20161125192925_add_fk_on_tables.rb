class AddFkOnTables < ActiveRecord::Migration[5.0]
  def change
    add_reference :generic_goals, :reference_model, foreign_key: true, index:true
    add_reference :generic_goals, :capacity_level, foreign_key: true, index:true
    add_reference :process_areas, :reference_model, foreign_key: true, index:true
    add_reference :process_areas, :maturity_level, foreign_key: true, index:true
    add_reference :process_areas, :category, foreign_key: true, index:true
    add_reference :specific_goals, :process_area, foreign_key: true, index:true
    add_reference :specific_practices, :specific_goal, foreign_key: true, index:true
    add_reference :work_products, :specific_practice, foreign_key: true, index:true
  end
end