class GenericGoalsController < ApplicationController
  before_action :set_generic_goal, only: [:show, :edit, :update, :destroy]

  # GET /generic_goals
  def index
    @generic_goals = GenericGoal.all
  end

  # GET /generic_goals/1
  def show
  end

  # GET /generic_goals/new
  def new
    @generic_goal = GenericGoal.new
  end

  # GET /generic_goals/1/edit
  def edit
  end

  # POST /generic_goals
  def create
    @generic_goal = GenericGoal.new(generic_goal_params)

    if @generic_goal.save
      redirect_to @generic_goal, notice: 'Generic goal was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /generic_goals/1
  def update
    if @generic_goal.update(generic_goal_params)
      redirect_to @generic_goal, notice: 'Generic goal was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /generic_goals/1
  def destroy
    @generic_goal.destroy
    redirect_to generic_goals_url, notice: 'Generic goal was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_generic_goal
      @generic_goal = GenericGoal.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def generic_goal_params
      params.require(:generic_goal).permit(:name, :abbrev, :description)
    end
end
