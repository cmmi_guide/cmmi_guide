class MaturityLevelsController < ApplicationController
  before_action :set_maturity_level, only: [:show, :edit, :update, :destroy]

  # GET /maturity_levels
  # GET /maturity_levels.json
  def index
    @maturity_levels = MaturityLevel.all
  end

  # GET /maturity_levels/1
  # GET /maturity_levels/1.json
  def show
  end

  # GET /maturity_levels/new
  def new
    @maturity_level = MaturityLevel.new
  end

  # GET /maturity_levels/1/edit
  def edit
  end

  # POST /maturity_levels
  # POST /maturity_levels.json
  def create
    @maturity_level = MaturityLevel.new(maturity_level_params)

    respond_to do |format|
      if @maturity_level.save
        format.html { redirect_to @maturity_level, notice: 'Maturity level was successfully created.' }
        format.json { render :show, status: :created, location: @maturity_level }
      else
        format.html { render :new }
        format.json { render json: @maturity_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /maturity_levels/1
  # PATCH/PUT /maturity_levels/1.json
  def update
    respond_to do |format|
      if @maturity_level.update(maturity_level_params)
        format.html { redirect_to @maturity_level, notice: 'Maturity level was successfully updated.' }
        format.json { render :show, status: :ok, location: @maturity_level }
      else
        format.html { render :edit }
        format.json { render json: @maturity_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /maturity_levels/1
  # DELETE /maturity_levels/1.json
  def destroy
    @maturity_level.destroy
    respond_to do |format|
      format.html { redirect_to maturity_levels_url, notice: 'Maturity level was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_maturity_level
      @maturity_level = MaturityLevel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def maturity_level_params
      params.require(:maturity_level).permit(:name, :abbrev, :description)
    end
end
