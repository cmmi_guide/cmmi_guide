class ReferenceModelsController < ApplicationController
  before_action :set_reference_model, only: [:show, :edit, :update, :destroy]

  # GET /reference_models
  # GET /reference_models.json
  def index
    @reference_models = ReferenceModel.all
  end

  # GET /reference_models/1
  # GET /reference_models/1.json
  def show
  end

  # GET /reference_models/new
  def new
    @reference_model = ReferenceModel.new
  end

  # GET /reference_models/1/edit
  def edit
  end

  # POST /reference_models
  # POST /reference_models.json
  def create
    @reference_model = ReferenceModel.new(reference_model_params)

    respond_to do |format|
      if @reference_model.save
        format.html { redirect_to @reference_model, notice: 'Reference model was successfully created.' }
        format.json { render :show, status: :created, location: @reference_model }
      else
        format.html { render :new }
        format.json { render json: @reference_model.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reference_models/1
  # PATCH/PUT /reference_models/1.json
  def update
    respond_to do |format|
      if @reference_model.update(reference_model_params)
        format.html { redirect_to @reference_model, notice: 'Reference model was successfully updated.' }
        format.json { render :show, status: :ok, location: @reference_model }
      else
        format.html { render :edit }
        format.json { render json: @reference_model.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reference_models/1
  # DELETE /reference_models/1.json
  def destroy
    @reference_model.destroy
    respond_to do |format|
      format.html { redirect_to reference_models_url, notice: 'Reference model was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reference_model
      @reference_model = ReferenceModel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reference_model_params
      params.require(:reference_model).permit(:abbrev, :name, :description)
    end
end
