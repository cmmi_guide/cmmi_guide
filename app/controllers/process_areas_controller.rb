class ProcessAreasController < ApplicationController
  before_action :set_process_area, only: [:show, :edit, :update, :destroy]

  # GET /process_areas
  # GET /process_areas.json
  def index
    @process_areas = ProcessArea.all
  end

  # GET /process_areas/1
  # GET /process_areas/1.json
  def show
  end

  # GET /process_areas/new
  def new
    @process_area = ProcessArea.new
  end

  # GET /process_areas/1/edit
  def edit
  end

  # POST /process_areas
  # POST /process_areas.json
  def create
    @process_area = ProcessArea.new(process_area_params)

    respond_to do |format|
      if @process_area.save
        format.html { redirect_to @process_area, notice: 'Process area was successfully created.' }
        format.json { render :show, status: :created, location: @process_area }
      else
        format.html { render :new }
        format.json { render json: @process_area.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /process_areas/1
  # PATCH/PUT /process_areas/1.json
  def update
    respond_to do |format|
      if @process_area.update(process_area_params)
        format.html { redirect_to @process_area, notice: 'Process area was successfully updated.' }
        format.json { render :show, status: :ok, location: @process_area }
      else
        format.html { render :edit }
        format.json { render json: @process_area.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /process_areas/1
  # DELETE /process_areas/1.json
  def destroy
    @process_area.destroy
    respond_to do |format|
      format.html { redirect_to process_areas_url, notice: 'Process area was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_process_area
      @process_area = ProcessArea.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def process_area_params
      params.require(:process_area).permit(:abbrev, :name, :description)
    end
end
