class WorkProductsController < ApplicationController
  before_action :set_work_product, only: [:show, :edit, :update, :destroy]

  # GET /work_products
  # GET /work_products.json
  def index
    @work_products = WorkProduct.all
  end

  # GET /work_products/1
  # GET /work_products/1.json
  def show
  end

  # GET /work_products/new
  def new
    @work_product = WorkProduct.new
  end

  # GET /work_products/1/edit
  def edit
  end

  # POST /work_products
  # POST /work_products.json
  def create
    @work_product = WorkProduct.new(work_product_params)

    respond_to do |format|
      if @work_product.save
        format.html { redirect_to @work_product, notice: 'Work product was successfully created.' }
        format.json { render :show, status: :created, location: @work_product }
      else
        format.html { render :new }
        format.json { render json: @work_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /work_products/1
  # PATCH/PUT /work_products/1.json
  def update
    respond_to do |format|
      if @work_product.update(work_product_params)
        format.html { redirect_to @work_product, notice: 'Work product was successfully updated.' }
        format.json { render :show, status: :ok, location: @work_product }
      else
        format.html { render :edit }
        format.json { render json: @work_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /work_products/1
  # DELETE /work_products/1.json
  def destroy
    @work_product.destroy
    respond_to do |format|
      format.html { redirect_to work_products_url, notice: 'Work product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_work_product
      @work_product = WorkProduct.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def work_product_params
      params.require(:work_product).permit(:name)
    end
end
