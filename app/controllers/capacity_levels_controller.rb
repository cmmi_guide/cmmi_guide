class CapacityLevelsController < ApplicationController
  before_action :set_capacity_level, only: [:show, :edit, :update, :destroy]

  # GET /capacity_levels
  # GET /capacity_levels.json
  def index
    @capacity_levels = CapacityLevel.all
  end

  # GET /capacity_levels/1
  # GET /capacity_levels/1.json
  def show
  end

  # GET /capacity_levels/new
  def new
    @capacity_level = CapacityLevel.new
  end

  # GET /capacity_levels/1/edit
  def edit
  end

  # POST /capacity_levels
  # POST /capacity_levels.json
  def create
    @capacity_level = CapacityLevel.new(capacity_level_params)

    respond_to do |format|
      if @capacity_level.save
        format.html { redirect_to @capacity_level, notice: 'Capacity level was successfully created.' }
        format.json { render :show, status: :created, location: @capacity_level }
      else
        format.html { render :new }
        format.json { render json: @capacity_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /capacity_levels/1
  # PATCH/PUT /capacity_levels/1.json
  def update
    respond_to do |format|
      if @capacity_level.update(capacity_level_params)
        format.html { redirect_to @capacity_level, notice: 'Capacity level was successfully updated.' }
        format.json { render :show, status: :ok, location: @capacity_level }
      else
        format.html { render :edit }
        format.json { render json: @capacity_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /capacity_levels/1
  # DELETE /capacity_levels/1.json
  def destroy
    @capacity_level.destroy
    respond_to do |format|
      format.html { redirect_to capacity_levels_url, notice: 'Capacity level was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_capacity_level
      @capacity_level = CapacityLevel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def capacity_level_params
      params.require(:capacity_level).permit(:name, :abbrev, :description)
    end
end
