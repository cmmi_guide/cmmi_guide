json.extract! maturity_level, :id, :name, :abbrev, :description, :created_at, :updated_at
json.url maturity_level_url(maturity_level, format: :json)