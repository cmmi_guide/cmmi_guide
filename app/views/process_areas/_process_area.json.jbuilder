json.extract! process_area, :id, :abbrev, :name, :description, :created_at, :updated_at
json.url process_area_url(process_area, format: :json)