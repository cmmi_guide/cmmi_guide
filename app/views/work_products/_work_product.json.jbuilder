json.extract! work_product, :id, :name, :created_at, :updated_at
json.url work_product_url(work_product, format: :json)