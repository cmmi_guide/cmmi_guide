json.extract! specific_goal, :id, :name, :abbrev, :description, :created_at, :updated_at
json.url specific_goal_url(specific_goal, format: :json)