json.extract! specific_practice, :id, :name, :abbrev, :description, :created_at, :updated_at
json.url specific_practice_url(specific_practice, format: :json)