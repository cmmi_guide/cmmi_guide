json.extract! reference_model, :id, :abbrev, :name, :description, :created_at, :updated_at
json.url reference_model_url(reference_model, format: :json)