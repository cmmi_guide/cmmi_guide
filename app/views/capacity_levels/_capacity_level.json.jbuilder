json.extract! capacity_level, :id, :name, :abbrev, :description, :created_at, :updated_at
json.url capacity_level_url(capacity_level, format: :json)