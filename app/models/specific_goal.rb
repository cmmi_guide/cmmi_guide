class SpecificGoal < ApplicationRecord
  belongs_to :specific_practice

  has_many :process_areas
end
