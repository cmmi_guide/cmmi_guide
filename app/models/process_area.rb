class ProcessArea < ApplicationRecord
  belongs_to :reference_model
  belongs_to :category
  belongs_to :maturity_level
end
