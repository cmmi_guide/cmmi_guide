# README #

# How to start? #

* Create a FORK
* Clone the repository (git clone FORK_PATH)
* Run server (rails server)

# How to commit? #

* Create a new branch (git checkout -b BRANCH_NAME)
* Make changes and include on git (git add . - or - git add FILES_PATH)
* Check your files (git status)
* Commit (git commit -m "what's wrong or what happened")

# How to send to server? #

* Check your commits and run "git push origin BRANCH_NAME"
* After commited, it will appear a link to create a pull request
* Copy and paste on Browser and complete the descriptions (Don't forget the reviewers)