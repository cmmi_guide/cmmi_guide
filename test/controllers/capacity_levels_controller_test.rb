require 'test_helper'

class CapacityLevelsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @capacity_level = capacity_levels(:one)
  end

  test "should get index" do
    get capacity_levels_url
    assert_response :success
  end

  test "should get new" do
    get new_capacity_level_url
    assert_response :success
  end

  test "should create capacity_level" do
    assert_difference('CapacityLevel.count') do
      post capacity_levels_url, params: { capacity_level: { abbrev: @capacity_level.abbrev, description: @capacity_level.description, name: @capacity_level.name } }
    end

    assert_redirected_to capacity_level_url(CapacityLevel.last)
  end

  test "should show capacity_level" do
    get capacity_level_url(@capacity_level)
    assert_response :success
  end

  test "should get edit" do
    get edit_capacity_level_url(@capacity_level)
    assert_response :success
  end

  test "should update capacity_level" do
    patch capacity_level_url(@capacity_level), params: { capacity_level: { abbrev: @capacity_level.abbrev, description: @capacity_level.description, name: @capacity_level.name } }
    assert_redirected_to capacity_level_url(@capacity_level)
  end

  test "should destroy capacity_level" do
    assert_difference('CapacityLevel.count', -1) do
      delete capacity_level_url(@capacity_level)
    end

    assert_redirected_to capacity_levels_url
  end
end
