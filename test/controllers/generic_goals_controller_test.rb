require 'test_helper'

class GenericGoalsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @generic_goal = generic_goals(:one)
  end

  test "should get index" do
    get generic_goals_url
    assert_response :success
  end

  test "should get new" do
    get new_generic_goal_url
    assert_response :success
  end

  test "should create generic_goal" do
    assert_difference('GenericGoal.count') do
      post generic_goals_url, params: { generic_goal: { abbrev: @generic_goal.abbrev, description: @generic_goal.description, name: @generic_goal.name } }
    end

    assert_redirected_to generic_goal_url(GenericGoal.last)
  end

  test "should show generic_goal" do
    get generic_goal_url(@generic_goal)
    assert_response :success
  end

  test "should get edit" do
    get edit_generic_goal_url(@generic_goal)
    assert_response :success
  end

  test "should update generic_goal" do
    patch generic_goal_url(@generic_goal), params: { generic_goal: { abbrev: @generic_goal.abbrev, description: @generic_goal.description, name: @generic_goal.name } }
    assert_redirected_to generic_goal_url(@generic_goal)
  end

  test "should destroy generic_goal" do
    assert_difference('GenericGoal.count', -1) do
      delete generic_goal_url(@generic_goal)
    end

    assert_redirected_to generic_goals_url
  end
end
