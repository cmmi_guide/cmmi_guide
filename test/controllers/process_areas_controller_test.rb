require 'test_helper'

class ProcessAreasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @process_area = process_areas(:one)
  end

  test "should get index" do
    get process_areas_url
    assert_response :success
  end

  test "should get new" do
    get new_process_area_url
    assert_response :success
  end

  test "should create process_area" do
    assert_difference('ProcessArea.count') do
      post process_areas_url, params: { process_area: { abbrev: @process_area.abbrev, description: @process_area.description, name: @process_area.name } }
    end

    assert_redirected_to process_area_url(ProcessArea.last)
  end

  test "should show process_area" do
    get process_area_url(@process_area)
    assert_response :success
  end

  test "should get edit" do
    get edit_process_area_url(@process_area)
    assert_response :success
  end

  test "should update process_area" do
    patch process_area_url(@process_area), params: { process_area: { abbrev: @process_area.abbrev, description: @process_area.description, name: @process_area.name } }
    assert_redirected_to process_area_url(@process_area)
  end

  test "should destroy process_area" do
    assert_difference('ProcessArea.count', -1) do
      delete process_area_url(@process_area)
    end

    assert_redirected_to process_areas_url
  end
end
