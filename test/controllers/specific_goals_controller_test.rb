require 'test_helper'

class SpecificGoalsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @specific_goal = specific_goals(:one)
  end

  test "should get index" do
    get specific_goals_url
    assert_response :success
  end

  test "should get new" do
    get new_specific_goal_url
    assert_response :success
  end

  test "should create specific_goal" do
    assert_difference('SpecificGoal.count') do
      post specific_goals_url, params: { specific_goal: { abbrev: @specific_goal.abbrev, description: @specific_goal.description, name: @specific_goal.name } }
    end

    assert_redirected_to specific_goal_url(SpecificGoal.last)
  end

  test "should show specific_goal" do
    get specific_goal_url(@specific_goal)
    assert_response :success
  end

  test "should get edit" do
    get edit_specific_goal_url(@specific_goal)
    assert_response :success
  end

  test "should update specific_goal" do
    patch specific_goal_url(@specific_goal), params: { specific_goal: { abbrev: @specific_goal.abbrev, description: @specific_goal.description, name: @specific_goal.name } }
    assert_redirected_to specific_goal_url(@specific_goal)
  end

  test "should destroy specific_goal" do
    assert_difference('SpecificGoal.count', -1) do
      delete specific_goal_url(@specific_goal)
    end

    assert_redirected_to specific_goals_url
  end
end
