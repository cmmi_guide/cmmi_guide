require 'test_helper'

class MaturityLevelsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @maturity_level = maturity_levels(:one)
  end

  test "should get index" do
    get maturity_levels_url
    assert_response :success
  end

  test "should get new" do
    get new_maturity_level_url
    assert_response :success
  end

  test "should create maturity_level" do
    assert_difference('MaturityLevel.count') do
      post maturity_levels_url, params: { maturity_level: { abbrev: @maturity_level.abbrev, description: @maturity_level.description, name: @maturity_level.name } }
    end

    assert_redirected_to maturity_level_url(MaturityLevel.last)
  end

  test "should show maturity_level" do
    get maturity_level_url(@maturity_level)
    assert_response :success
  end

  test "should get edit" do
    get edit_maturity_level_url(@maturity_level)
    assert_response :success
  end

  test "should update maturity_level" do
    patch maturity_level_url(@maturity_level), params: { maturity_level: { abbrev: @maturity_level.abbrev, description: @maturity_level.description, name: @maturity_level.name } }
    assert_redirected_to maturity_level_url(@maturity_level)
  end

  test "should destroy maturity_level" do
    assert_difference('MaturityLevel.count', -1) do
      delete maturity_level_url(@maturity_level)
    end

    assert_redirected_to maturity_levels_url
  end
end
