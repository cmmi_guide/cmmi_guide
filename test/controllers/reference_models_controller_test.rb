require 'test_helper'

class ReferenceModelsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @reference_model = reference_models(:one)
  end

  test "should get index" do
    get reference_models_url
    assert_response :success
  end

  test "should get new" do
    get new_reference_model_url
    assert_response :success
  end

  test "should create reference_model" do
    assert_difference('ReferenceModel.count') do
      post reference_models_url, params: { reference_model: { abbrev: @reference_model.abbrev, description: @reference_model.description, name: @reference_model.name } }
    end

    assert_redirected_to reference_model_url(ReferenceModel.last)
  end

  test "should show reference_model" do
    get reference_model_url(@reference_model)
    assert_response :success
  end

  test "should get edit" do
    get edit_reference_model_url(@reference_model)
    assert_response :success
  end

  test "should update reference_model" do
    patch reference_model_url(@reference_model), params: { reference_model: { abbrev: @reference_model.abbrev, description: @reference_model.description, name: @reference_model.name } }
    assert_redirected_to reference_model_url(@reference_model)
  end

  test "should destroy reference_model" do
    assert_difference('ReferenceModel.count', -1) do
      delete reference_model_url(@reference_model)
    end

    assert_redirected_to reference_models_url
  end
end
