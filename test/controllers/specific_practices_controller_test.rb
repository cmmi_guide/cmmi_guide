require 'test_helper'

class SpecificPracticesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @specific_practice = specific_practices(:one)
  end

  test "should get index" do
    get specific_practices_url
    assert_response :success
  end

  test "should get new" do
    get new_specific_practice_url
    assert_response :success
  end

  test "should create specific_practice" do
    assert_difference('SpecificPractice.count') do
      post specific_practices_url, params: { specific_practice: { abbrev: @specific_practice.abbrev, description: @specific_practice.description, name: @specific_practice.name } }
    end

    assert_redirected_to specific_practice_url(SpecificPractice.last)
  end

  test "should show specific_practice" do
    get specific_practice_url(@specific_practice)
    assert_response :success
  end

  test "should get edit" do
    get edit_specific_practice_url(@specific_practice)
    assert_response :success
  end

  test "should update specific_practice" do
    patch specific_practice_url(@specific_practice), params: { specific_practice: { abbrev: @specific_practice.abbrev, description: @specific_practice.description, name: @specific_practice.name } }
    assert_redirected_to specific_practice_url(@specific_practice)
  end

  test "should destroy specific_practice" do
    assert_difference('SpecificPractice.count', -1) do
      delete specific_practice_url(@specific_practice)
    end

    assert_redirected_to specific_practices_url
  end
end
