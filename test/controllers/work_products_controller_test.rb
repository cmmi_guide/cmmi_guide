require 'test_helper'

class WorkProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @work_product = work_products(:one)
  end

  test "should get index" do
    get work_products_url
    assert_response :success
  end

  test "should get new" do
    get new_work_product_url
    assert_response :success
  end

  test "should create work_product" do
    assert_difference('WorkProduct.count') do
      post work_products_url, params: { work_product: { name: @work_product.name } }
    end

    assert_redirected_to work_product_url(WorkProduct.last)
  end

  test "should show work_product" do
    get work_product_url(@work_product)
    assert_response :success
  end

  test "should get edit" do
    get edit_work_product_url(@work_product)
    assert_response :success
  end

  test "should update work_product" do
    patch work_product_url(@work_product), params: { work_product: { name: @work_product.name } }
    assert_redirected_to work_product_url(@work_product)
  end

  test "should destroy work_product" do
    assert_difference('WorkProduct.count', -1) do
      delete work_product_url(@work_product)
    end

    assert_redirected_to work_products_url
  end
end
