Rails.application.routes.draw do
  resources :work_products
  resources :categories
  resources :reference_models
  resources :process_areas
  get 'static_pages/home'

  resources :capacity_levels
  resources :maturity_levels
  resources :specific_goals
  resources :specific_practices
  resources :generic_goals
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'static_pages#home'
end
